
package smtpd

import (
  "fmt"

  "github.com/yazgazan/ysmtpd/parseUtils"
)

func HookQuit(buff *parseUtils.Parser, conn *Connection) error {
  conn.Write([]byte("221 Service closing transmission channel\r\n"))
  conn.Close()
  return fmt.Errorf("quitting as requested by the client")
}

