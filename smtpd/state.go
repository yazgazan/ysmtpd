
package smtpd

type State struct{
  From  string
  To    map[int]string
  Data  []byte
}

func MakeState() *State {
  return &State{
    From: "",
    To: make(map[int]string),
  }
}

