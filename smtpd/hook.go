
package smtpd

import (
  "syscall"

  "github.com/yazgazan/ysmtpd/parseUtils"
)

type HookFn func (buff *parseUtils.Parser, conn *Connection) error

type Hook struct{
  Func  HookFn
}

type HookCollection map[string]*Hook

func (hc *HookCollection) Set(name string, hook *Hook) {
  (*hc)[name] = hook
}

func (hc *HookCollection) Add(name string, fn HookFn) *Hook {
  newHook := &Hook{
    Func: fn,
  }

  (*hc)[name] = newHook

  return newHook
}

func (hc *HookCollection) Remove(name string) {
  _, ok := (*hc)[name]

  if ok == false {
    return
  }

  (*hc)[name] = nil
}

func (hc *HookCollection) Exists(name string) bool {
  hook, ok := (*hc)[name]

  if ok == false {
    return false
  }
  if hook == nil {
    return false
  }
  return true
}

func (hc *HookCollection) Get(name string) (*Hook, error) {
  if hc.Exists(name) == false {
    return nil, syscall.ENOKEY
  }

  hook, _ := (*hc)[name]
  return hook, nil
}

func (hc *HookCollection) Trigger(name string, buff *parseUtils.Parser, conn *Connection,
) error {
  hook, err := hc.Get(name)

  if err != nil {
    return err
  }
  return hook.Func(buff, conn)
}

func DefaultHooks() HookCollection {
  ret := make(HookCollection)

  ret.Add("greetings", HookGreetings)
  ret.Add("HELO", HookHelo)
  ret.Add("EHLO", HookEhlo)
  ret.Add("MAIL", HookMail)
  ret.Add("RCPT", HookRcpt)
  ret.Add("DATA", HookData)
  ret.Add("RSET", HookRset)
  ret.Add("NOOP", HookNoop)
  ret.Add("VRFY", HookVrfy)
  ret.Add("HELP", HookHelp)
  ret.Add("QUIT", HookQuit)
  return ret
}

