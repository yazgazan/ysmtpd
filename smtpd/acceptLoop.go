
package smtpd

import (
  "fmt"
  "net"
  "syscall"
)

func (s *Server) AcceptLoop() error {

  for {
    conn, err := s.Listener.Accept()

    if err != nil {
      if err == syscall.EINVAL {
        return err
      }
      fmt.Println(err)
      continue
    }

    err = s.NewConnectionHandler(conn)

    if err != nil {
      fmt.Println(err)
      continue
    }
  }
  return nil
}

func (s *Server) NewConnectionHandler(conn net.Conn) error {
  connection := MakeConnection(conn)
  err := s.Hooks.Trigger("new-connection", nil, connection)

  if (err != nil) && (err != syscall.ENOKEY) {
    conn.Close()
    return err
  }

  go connection.Run(s.Hooks, s.Conf)
  return nil
}

