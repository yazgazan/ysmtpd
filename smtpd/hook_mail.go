
package smtpd

import (
  "fmt"

  "github.com/yazgazan/ysmtpd/parseUtils"
)

func HookMail(buff *parseUtils.Parser, conn *Connection) error {
  conn.Write([]byte("502 Command not implemented\r\n"))
  fmt.Println("MAIL : command not implemented")
  return nil
}

