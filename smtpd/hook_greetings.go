
package smtpd

import (
  "github.com/yazgazan/ysmtpd/parseUtils"
)

func HookGreetings(buff *parseUtils.Parser, conn *Connection) error {
  conn.Write([]byte("220 Simple Mail Transfer Service Ready\r\n"))
  return nil
}

