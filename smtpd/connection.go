
package smtpd

import (
  "io"
  "net"
  "fmt"
  "strings"
  "syscall"

  "github.com/yazgazan/ysmtpd/parseUtils"
)

type Connection struct{
  net.Conn
  State *State
  Buffer *parseUtils.Parser
}

func MakeConnection(conn net.Conn) *Connection {
  return &Connection{
    conn,
    MakeState(),
    parseUtils.MakeParser(),
  }
}

func (c *Connection) Run(hooks HookCollection, conf *Conf) {
  err := hooks.Trigger("greetings", nil, c)

  if (err != nil) && (err != syscall.ENOKEY) {
    if err := hooks.Trigger("closing", nil, c);
    (err != nil) && (err != syscall.ENOKEY) {
      fmt.Println(err)
    }
    c.Close()
    return
  }

  for {
    buff := make([]byte, conf.ReadSize)
    n, err := c.Read(buff)

    if n > 0 {
      c.Buffer.Write(buff[:n])
    }
    // try parse & dispatch
    err2 := c.ReadLines(hooks, conf)
    if err2 != nil {
      fmt.Println(err2)
      break
    }
    if err != nil {
      if err != io.EOF {
        fmt.Println(err)
      }
      err = c.Close()
      if err != nil {
        fmt.Println(err)
      }
      break
    }
  }
  if err := hooks.Trigger("closed", nil, c);
  (err != nil) && (err != syscall.ENOKEY) {
    fmt.Println(err)
  }
}

func getCmd(buff *parseUtils.Parser) (string, error) {
  var err error = nil

  buff.SetMark("line-start")
  ok := buff.ReadUntil([]byte(" "))

  if ok == true {
    ret, err := buff.StringFromMark("line-start")
    if err == nil {
      if buff.ReadByte(' ') == false {
        err = fmt.Errorf("something went wrong with the line buffer")
      }
    }
    return ret, err
  }

  if buff.Len() == 0 {
    err = fmt.Errorf("buffer is empty")
  }
  return buff.String(), err
}

func (c *Connection) ParseAndDispatch(
  hooks HookCollection,
  conf *Conf,
  buff *parseUtils.Parser) error {
    cmd, err := getCmd(buff)

    if err != nil {
      return err
    }
    cmd = strings.ToUpper(cmd)
    err = hooks.Trigger(cmd, buff, c)

    if err != nil {
      if err == syscall.ENOKEY {
        c.Write([]byte("500 Command not recognized\r\n"))
        return nil
      } else {
        return err
      }
    }
    fmt.Printf("'%s'\n", cmd)
    return nil
}

func (c *Connection) ReadLines(hooks HookCollection, conf *Conf) error {
  for {
    c.Buffer.SetMark("buff-start")
    ok := c.Buffer.ReadUntil([]byte("\r\n"))

    if ok == false {
      break
    }
    line, err := c.Buffer.ParserFromMark("buff-start")

    if err != nil {
      fmt.Println(err)
      if err := hooks.Trigger("closing", nil, c);
      (err != nil) && (err != syscall.ENOKEY) {
        fmt.Println(err)
      }
      return err
    }
    err = c.ParseAndDispatch(hooks, conf, line)
    if err != nil {
      return err
    }
    if c.Buffer.ReadBytes([]byte("\r\n")) == false {
      if err := hooks.Trigger("closing", nil, c);
      (err != nil) && (err != syscall.ENOKEY) {
        fmt.Println(err)
      }
      return fmt.Errorf("something went wrong with the buffer")
    }
  }
  return nil
}

