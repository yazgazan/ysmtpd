
package smtpd

import (
  "net"
)

type Server struct{
  Hooks     HookCollection
  Listener  net.Listener
  Conf      *Conf
}

func MakeServer() *Server {
  return &Server{
    Hooks:  DefaultHooks(),
    Conf:   MakeConf(),
  }
}

func (s *Server) Start(netType string, addr string) error {
  var err error

  s.Listener, err = net.Listen(netType, addr)
  if err != nil {
    return err
  }
  return s.AcceptLoop()
}

func (s *Server) StartTCP(addr string) error {
  var err error

  s.Listener, err = net.Listen("tcp", addr)
  if err != nil {
    return err
  }
  return s.AcceptLoop()
}

func (s *Server) StartTCP4(addr string) error {
  var err error

  s.Listener, err = net.Listen("tcp4", addr)
  if err != nil {
    return err
  }
  return s.AcceptLoop()
}

func (s *Server) StartTCP6(addr string) error {
  var err error

  s.Listener, err = net.Listen("tcp6", addr)
  if err != nil {
    return err
  }
  return s.AcceptLoop()
}

func (s *Server) StartUnix(addr string) error {
  var err error

  s.Listener, err = net.Listen("unix", addr)
  if err != nil {
    return err
  }
  return s.AcceptLoop()
}

func (s *Server) StartUnixPacket(addr string) error {
  var err error

  s.Listener, err = net.Listen("unixpacket", addr)
  if err != nil {
    return err
  }
  return s.AcceptLoop()
}

func (s *Server) StartListener(listener net.Listener) error {
  s.Listener = listener

  return s.AcceptLoop()
}

