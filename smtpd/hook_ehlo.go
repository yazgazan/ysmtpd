
package smtpd

import (
  "fmt"

  "github.com/yazgazan/ysmtpd/parseUtils"
)

func HookEhlo(buff *parseUtils.Parser, conn *Connection) error {
  conn.Write([]byte("502 Command not implemented\r\n"))
  fmt.Println("EHLO : command not implemented")
  return nil
}

