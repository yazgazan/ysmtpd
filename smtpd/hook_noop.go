
package smtpd

import (
  "github.com/yazgazan/ysmtpd/parseUtils"
)

func HookNoop(buff *parseUtils.Parser, conn *Connection) error {
  conn.Write([]byte("250 ok\r\n"))
  return nil
}

