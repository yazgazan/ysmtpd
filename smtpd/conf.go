
package smtpd

type Conf struct{
  ReadSize  int64
}

func MakeConf() *Conf {
  return &Conf{
    ReadSize: C_defaultReadSize,
  }
}

