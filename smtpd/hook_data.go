
package smtpd

import (
  "fmt"

  "github.com/yazgazan/ysmtpd/parseUtils"
)

func HookData(buff *parseUtils.Parser, conn *Connection) error {
  conn.Write([]byte("502 Command not implemented\r\n"))
  fmt.Println("DATA : command not implemented")
  return nil
}

