
package parseUtils

import (
  "fmt"
  "bytes"
)

type Parser struct{
  bytes.Buffer
  pos     int
  marks   map[string]int
}

func MakeParser() *Parser {
  return &Parser{
    pos: 0,
    marks: make(map[string]int),
  }
}

func (p *Parser) SetMark(name string) {
  p.marks[name] = p.pos
}

func (p *Parser) GetMark(name string) (int, error) {
  pos, ok := p.marks[name]

  if ok == false {
    return 0, fmt.Errorf("no such mark")
  }
  return pos, nil
}

func (p *Parser) BytesFromMark(name string) ([]byte, error) {
  start, err := p.GetMark(name)

  if err != nil {
    return nil, err
  }
  return p.Bytes()[start:p.pos], nil
}

func (p *Parser) StringFromMark(name string) (string, error) {
  start, err := p.GetMark(name)

  if err != nil {
    return "", err
  }
  return string(p.Bytes()[start:p.pos]), nil
}

func (p *Parser) ParserFromMark(name string) (*Parser, error) {
  start, err := p.GetMark(name)

  if err != nil {
    return nil, err
  }
  ret := MakeParser()
  ret.Write(p.Bytes()[start:p.pos])
  return ret, nil
}

func (p *Parser) Remaining() []byte {
  return p.Buffer.Bytes()[p.pos:]
}

func (p *Parser) String() string {
  return p.Buffer.String()[p.pos:]
}

func (p *Parser) ReadCmd() bool {
  n := bytes.IndexByte(p.Remaining(), ' ')

  if n != -1 {
    p.pos += n
    return true
  }
  n = bytes.Index(p.Remaining(), []byte("\r\n"))
  if n != -1 {
    p.pos += n
    return true
  }
  return false
}

func (p *Parser) ReadUntil(needle []byte) bool {
  n := bytes.Index(p.Remaining(), needle)

  if n != -1 {
    p.pos += n
    return true
  }
  return false
}

func (p *Parser) ReadBytes(b []byte) bool {
  n := bytes.Index(p.Remaining(), b)

  if n != 0 {
    return false
  }
  p.pos += len(b)
  return true
}

func (p *Parser) ReadByte(b byte) bool {
  if p.Bytes()[p.pos] == b {
    p.pos += 1
    return true
  }
  return false
}

func (p *Parser) Len() int {
  return p.Buffer.Len() - p.pos
}

