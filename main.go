package main

import (
  "fmt"

  "github.com/yazgazan/ysmtpd/smtpd"
  "github.com/yazgazan/ysmtpd/parseUtils"
)

func main() {
  server := smtpd.MakeServer()

  server.Hooks.Add("new-connection",
    func (cmd *parseUtils.Parser, conn *smtpd.Connection) error {
      fmt.Println("ha")
      return nil
    },)

  server.StartTCP4("127.0.0.1:1234")
}
